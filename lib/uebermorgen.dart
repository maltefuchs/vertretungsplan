import 'main.dart';
import 'package:flutter/material.dart';


class UebermorgenState extends State<Uebermorgen> {

  var _inhalt = [
    "bla",
    "blub",
    "bli",
    "blalvlsebs",
    "vsbrbsb4sg",
    "vsnvsvs",
    "bla",
    "blub",
    "bli",
    "blalvlsebs",
    "vsbrbsb4sg",
    "vsnvsvs",
    "bla",
    "blub",
    "bli",
    "blalvlsebs",
    "vsbrbsb4sg",
    "vsnvsvs",
    "bla",
    "blub",
    "bli",
    "blalvlsebs",
    "vsbrbsb4sg",
    "vsnvsvs",
    "bla",
    "blub",
    "bli",
    "blalvlsebs",
    "vsbrbsb4sg",
    "vsnvsvs",
    "bla",
    "blub",
    "bli",
    "blalvlsebs",
    "vsbrbsb4sg",
    "vsnvsvs",
  ];

  MyApp myApp = new MyApp();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: RefreshIndicator(
          child: _buildListe(),
          onRefresh: MainState().handleRefresh,
        )
    );
  }

  Widget _buildListe() {
    return ListView.builder(
        itemCount: (_inhalt.length),
        padding: const EdgeInsets.all(8.0),
        itemBuilder: (context, i) {
          //      if (i.isOdd) return Divider(height: 1);

          //      final index = i ~/ 2;

          return _buildReihe(_inhalt[i]);
        }
    );
  }

  Widget _buildReihe(var tile) {
    return Card(
      color: Colors.grey,
      child: ListTile(
        title: Text(
          tile,
          style: TextStyle(fontSize: 18.0),
        ),
        leading: Text("1./2.", style: TextStyle(fontSize: 30.0, fontWeight: FontWeight.bold, height: 1.2)),
        subtitle: Text("Klasse frei"),
        trailing: Icon(Icons.chevron_right),
        onTap: _pushTile,
      ),
    );

  }

  void _pushTile() {
    Navigator.of(context).push(
      MaterialPageRoute<void>(
        builder: (BuildContext context) {
          return Scaffold(
            appBar: AppBar(
              title: Text('Vertretung 1'),
              actions: <Widget>[
                IconButton(
                  icon: Icon(Icons.share),
                  tooltip: 'Teile die Vertretung',
                  onPressed: () {},
                ),
              ],
            ),
            body: Text("gse"),
          );
        },
      ),
    );
  }

}

class Uebermorgen extends StatefulWidget {
  @override
  UebermorgenState createState() => UebermorgenState();
}