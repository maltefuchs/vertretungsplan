
class VDaten {

  final String schule;
  final String datum;
  final String klasse;
  final String stunde;
  final String lehrer;
  final String fach;
  final String raum;
  final String vlehrer;
  final String vfach;
  final String vraum;
  final String merkmal;
  final String info;

  VDaten(
      this.schule,
      this.datum,
      this.klasse,
      this.stunde,
      this.lehrer,
      this.fach,
      this.raum,
      this.vlehrer,
      this.vfach,
      this.vraum,
      this.merkmal,
      this.info
      );

}