import 'package:share/share.dart';

import 'main.dart';
import 'test.dart';
import 'package:flutter/material.dart';


class HeuteState extends State<Heute> {

  TestState testState = TestState();
  var _inhalt;
  MyApp myApp = new MyApp();

  @override
  void initState() {
    super.initState();
    _getData();
  }

  _getData() {

  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: RefreshIndicator(
          child: _buildListe(),
          onRefresh: MainState().handleRefresh,
        )
    );
  }

  Widget _buildListe() {
    return ListView.builder(
      itemCount: (_inhalt.length),
      padding: const EdgeInsets.all(8.0),
      itemBuilder: (context, i) {
  //      if (i.isOdd) return Divider(height: 1);

  //      final index = i ~/ 2;

        return _buildReihe(_inhalt[i]);
      }
    );
  }

  Widget _buildReihe(var tile) {
    return Card(
      color: Colors.grey,
        child: ListTile(
          title: Text(
            tile,
            style: TextStyle(fontSize: 18.0),
          ),
          leading: Text("1./2.", style: TextStyle(fontSize: 30.0, fontWeight: FontWeight.bold, height: 1.2)),
          subtitle: Text("Klasse frei"),
          trailing: Icon(Icons.chevron_right),
          onTap: _pushTile,
        ),
    );

  }

  void _pushTile() {
    Navigator.of(context).push(
      MaterialPageRoute<void>(
        builder: (BuildContext context) {
          return Scaffold(
            appBar: AppBar(
              title: Text('Vertretung 1'),
              actions: <Widget>[
                IconButton(
                    icon: Icon(Icons.share),
                    tooltip: 'Teile die Vertretung',
                    onPressed: _share,
                ),
              ],
            ),
            body: Text("gse"),
          );
        },
      ),
    );
  }

  void _share() {
    Share.share("YEEES!!!\nIrgendwas ist anders... Aber was?\nSchau in der FLS Vertretungsplan-App nach!\nhttps://app.bla/");
  }
}


class Heute extends StatefulWidget {
  @override
  HeuteState createState() => HeuteState();
}