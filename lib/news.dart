import 'dart:io';
import 'package:flutter/cupertino.dart';
import 'package:vertretungplan/res/colors.dart';
import 'main.dart';
import 'package:flutter/material.dart';
import 'dart:async';
import 'package:http/http.dart' as http;
import 'package:xml/xml.dart' as xml;
import 'package:indexed_list_view/indexed_list_view.dart';
import 'package:progress_dialog/progress_dialog.dart';

class NewsState extends State<News> {

  static IndexedScrollController controller = IndexedScrollController(initialIndex: 75);

  MyApp myApp = new MyApp();

  var list = List();
  var listFormatting;

  var refreshKey = GlobalKey<RefreshIndicatorState>();

  void initState() {
    super.initState();
    handleRefreshOnStart();
  }

  parsing(var xmlIn) {
    var document = xml.parse(xmlIn);
    //print(document.toString());
    //print(document.toXmlString(pretty: true, indent: '\t'));
    Iterable<xml.XmlElement> items = document.findAllElements('class');
    items.map((xml.XmlElement item) {
      var bar = getValue(item.findElements("date"));
   //   var bar_desc = getValue(item.findElements("BAR_DESC"));
   //   var qty = getValue(item.findElements("QTY"));
   //   var price = getValue(item.findElements("PRICE"));
      list.add(bar);
    }).toList();
  }

  getValue(Iterable<xml.XmlElement> items) {
    var textValue;
    items.map((xml.XmlElement node) {
      textValue = node.text;
    }).toList();
    //print("get value");
    return textValue;
  }

  Future<Null> handleRefresh() async {
    var temp = await http.read("http://fls-wiesbaden.de/raw/vplan");
    refreshKey.currentState?.show(atTop: false);
    await new Future.delayed(new Duration(seconds: 1));
    setState(() {
      parsing(temp);
    });
    return null;
  }

  Future<Null> handleRefreshOnStart() async {

    ProgressDialog pr;
    pr = new ProgressDialog(context, type: ProgressDialogType.Normal, isDismissible: false);


    String url = "http://fls-wiesbaden.de/raw/vplan";
    var temp;

    Future.delayed(Duration.zero, () async {
      pr.show();
      temp = await http.read(url);
    }).then((value) {
      pr.hide().whenComplete(() {
        setState(() {
          parsing(temp);
        });
      });
    });

   // setState(() {
   //   parsing(document);
   // });

    return null;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: RefreshIndicator(
          key: refreshKey,
          child: _buildListe(),
          onRefresh: handleRefresh,
        ),
    );
  }

  Widget _buildListe() {
    return ListView.builder(
        physics: BouncingScrollPhysics(),
        itemCount: list?.length,
         padding: const EdgeInsets.all(8.0),
        itemBuilder: (context, i) {
          return _buildReihe(list[i].toString());
        }
    );
  }

  Widget _buildReihe(var tile) {
    return Card(
      color: Colors.grey,
      child: ListTile(
        title: Text(
          tile,
          style: TextStyle(fontSize: 18.0),
        ),
        leading: Icon(CupertinoIcons.news, size: 48.0, color: Colors.black,),
        subtitle: Text("dies das"),
        trailing: Icon(Icons.chevron_right),
        onTap: _pushTile,
      ),
    );
  }

  void _pushTile() {
    Navigator.of(context).push(
      MaterialPageRoute<void>(
        builder: (BuildContext context) {
          return Scaffold(
            appBar: AppBar(
              title: Text('News 1'),
              actions: <Widget>[
                IconButton(
                  icon: Icon(Icons.share),
                  tooltip: 'Teile die Neuigkeit',
                  onPressed: () {},
                ),
              ],
            ),
            body: Text("gse"),
          );
        },
      ),
    );
  }

}

class News extends StatefulWidget {
  @override
  NewsState createState() => NewsState();
}