import 'dart:io';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:vertretungplan/test.dart';
import 'package:vertretungplan/settings.dart';
import 'package:vertretungplan/news.dart';
import 'package:vertretungplan/heute.dart';
import 'package:vertretungplan/morgen.dart';
import 'package:vertretungplan/uebermorgen.dart';
import 'package:vertretungplan/res/colors.dart';


void main() => runApp(MyApp());
Settings settings = Settings();

class MyApp extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Vertretungsplan FLS',
      debugShowCheckedModeBanner: false,
      home: Main(),
    );
  }
}

class MainState extends State<Main>{

  /// DATUMSANZEIGE

  var date1;
  var date2;
  var date3;
  var date1AsDateTime;
  var date2AsDateTime;
  var date3AsDateTime;
  String date1dayAsString;
  String date2dayAsString;
  String date3dayAsString;
  List<String> datesAsList = new List<String>();

  dates() {
    switch (DateTime.now().weekday.toInt()) {
      case 4:
        date1AsDateTime = new DateTime.now();
        date2AsDateTime = new DateTime.now().add(Duration(days: 1));
        date3AsDateTime = new DateTime.now().add(Duration(days: 4));
        date1 = "Heute";
        date2 = weekdayNrToStr(date2AsDateTime.weekday.toInt()) + ", " + date2AsDateTime.day.toString() + "." + date2AsDateTime.month.toString() + "." + date2AsDateTime.year.toString();
        date3 = weekdayNrToStr(date3AsDateTime.weekday.toInt()) + ", " + date3AsDateTime.day.toString() + "." + date3AsDateTime.month.toString() + "." + date3AsDateTime.year.toString();
        break;
      case 5:
        date1AsDateTime = new DateTime.now();
        date2AsDateTime = new DateTime.now().add(Duration(days: 3));
        date3AsDateTime = new DateTime.now().add(Duration(days: 4));
        date1 = "Heute";
        date2 = weekdayNrToStr(date2AsDateTime.weekday.toInt()) + ", " + date2AsDateTime.day.toString() + "." + date2AsDateTime.month.toString() + "." + date2AsDateTime.year.toString()[2] + date2AsDateTime.year.toString()[3];
        date3 = weekdayNrToStr(date3AsDateTime.weekday.toInt()) + ", " + date3AsDateTime.day.toString() + "." + date3AsDateTime.month.toString() + "." + date3AsDateTime.year.toString()[2] + date3AsDateTime.year.toString()[3];
        break;
      case 6:
        date1AsDateTime = new DateTime.now().add(Duration(days: 2));
        date2AsDateTime = new DateTime.now().add(Duration(days: 3));
        date3AsDateTime = new DateTime.now().add(Duration(days: 4));
        date1 = weekdayNrToStr(date3AsDateTime.weekday.toInt()) + ", " + date1AsDateTime.day.toString() + "." + date3AsDateTime.month.toString() + "." + date3AsDateTime.year.toString()[2] + date3AsDateTime.year.toString()[3];
        date2 = weekdayNrToStr(date3AsDateTime.weekday.toInt()) + ", " + date2AsDateTime.day.toString() + "." + date3AsDateTime.month.toString() + "." + date3AsDateTime.year.toString()[2] + date3AsDateTime.year.toString()[3];
        date3 = weekdayNrToStr(date3AsDateTime.weekday.toInt()) + ", " + date3AsDateTime.day.toString() + "." + date3AsDateTime.month.toString() + "." + date3AsDateTime.year.toString()[2] + date3AsDateTime.year.toString()[3];
        break;
      case 7:
        date1AsDateTime = new DateTime.now().add(Duration(days: 1));
        date2AsDateTime = new DateTime.now().add(Duration(days: 2));
        date3AsDateTime = new DateTime.now().add(Duration(days: 3));
        date1 = weekdayNrToStr(date3AsDateTime.weekday.toInt()) + ", " + date3AsDateTime.day.toString() + "." + date3AsDateTime.month.toString() + "." + date3AsDateTime.year.toString()[2] + date3AsDateTime.year.toString()[3];
        date2 = weekdayNrToStr(date3AsDateTime.weekday.toInt()) + ", " + date3AsDateTime.day.toString() + "." + date3AsDateTime.month.toString() + "." + date3AsDateTime.year.toString()[2] + date3AsDateTime.year.toString()[3];
        date3 = weekdayNrToStr(date3AsDateTime.weekday.toInt()) + ", " + date3AsDateTime.day.toString() + "." + date3AsDateTime.month.toString() + "." + date3AsDateTime.year.toString()[2] + date3AsDateTime.year.toString()[3];
        break;
      default:
        date1AsDateTime = new DateTime.now();
        date2AsDateTime = new DateTime.now().add(Duration(days: 1));
        date3AsDateTime = new DateTime.now().add(Duration(days: 2));
        date1 = "Heute";
        date2 = weekdayNrToStr(date2AsDateTime.weekday.toInt()) + ", " + date3AsDateTime.day.toString() + "." + date3AsDateTime.month.toString() + "." + date3AsDateTime.year.toString()[2] + date3AsDateTime.year.toString()[3];
        date3 = weekdayNrToStr(date3AsDateTime.weekday.toInt()) + ", " + date3AsDateTime.day.toString() + "." + date3AsDateTime.month.toString() + "." + date3AsDateTime.year.toString()[2] + date3AsDateTime.year.toString()[3];
        break;
    }

    date1dayAsString = date1AsDateTime.day.toString();
    date2dayAsString = date2AsDateTime.day.toString();
    date3dayAsString = date3AsDateTime.day.toString();

    datesAsList.addAll([
      date1dayAsString,
      date2dayAsString,
      date3dayAsString,
    ]);


    return datesAsList;

  }

  @override
  Widget build(BuildContext context) {

    dates();

    return DefaultTabController(
      length: 4,
      child: Scaffold(
        appBar: AppBar(
          centerTitle: true,
          leading: IconButton(icon: Icon(Icons.settings), color: Colors.white, onPressed: () {
            Navigator.of(context).pushReplacement(
                MaterialPageRoute(
                  builder: (context) => Settings(),
                ));
          }),
          actions: <Widget>[
            IconButton(icon: Icon(Icons.filter_list, color: Colors.white), onPressed: () {showFilter(context);}),

          ],
          bottom: TabBar(
            isScrollable: true,
            tabs: [
              Tab(text: 'Test'),
              Tab(text: date1),
              Tab(text: date2),
              Tab(text: date3),
            ],
          ),
          title: Text('Vertretungplan FLS'),
        ),
        body: TabBarView(
          children: <Widget>[
            Test(),
            Heute(),
            Morgen(),
            Uebermorgen(),
          ],
        ),
      ),
    );
  }

  /// AKUTUALISIERUNG

  Future<Null> handleRefresh() async {
    await new Future.delayed(new Duration(seconds: 2));
    return null;
  }


  /// FILTER POPUP

  showFilter(BuildContext context) {
    Widget saveButton = FlatButton(
      child: Text("Speichern"),
      onPressed: () {
        Navigator.pop(context);
      },
    );
    Widget cancelButton = FlatButton(
      child: Text("Abbruch"),
      onPressed: () {
        Navigator.pop(context);
      },
    );
    //   print(list[2].toString());
    AlertDialog alert = AlertDialog(
      title: Text("Filter"),
      content: Text(
        "lorem ipsum",
      ),
      actions: [
        cancelButton,
        saveButton,
      ],
    );
    showDialog(
      barrierDismissible: false,
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }


  /// WOCHENTAGSUMWANDLUNG

  weekdayNrToStr(int number) {
    String weekday;

    switch (number) {
      case 1:
        weekday = "Mo";
        break;
      case 2:
        weekday = "Di";
        break;
      case 3:
        weekday = "Mi";
        break;
      case 4:
        weekday = "Do";
        break;
      case 5:
        weekday = "Fr";
        break;
      case 6:
        weekday = "Sa";
        break;
      case 7:
        weekday = "So";
        break;
    }

    return weekday;
  }

}


/// STATEFUL UTIL

class Main extends StatefulWidget {
  @override
  MainState createState() => MainState();
}


/// KONSTANTEN DEKLARATION

class Consts {
  Consts._();

  static const double padding = 16.0;
  static const double avatarRadius = 66.0;
}
