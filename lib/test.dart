import 'dart:async';
import 'dart:io';

import 'package:progress_dialog/progress_dialog.dart';
import 'main.dart';
import 'package:vertretungplan/VDaten.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:xml/xml.dart' as xml;
import 'package:http/http.dart' as http;

class TestState extends State<Test> {
  var list = List();

  //dynamic aktuelleDaten = "2;2020-02-12;1261;2|08:45:00|09:30:00;J?rgen|Morbe|MORB;BBUN|BBUN;A305  SB;||;|;null;verschoben auf Mi, 12.02. 4. Std.;null\n3;2020-02-10;1082;6|12:15:00|13:00:00;Brigitte|Schuchert|SCHT;KLSTD|KLSTD;A105 DV SB;||;|;null;null;Klasse frei\n1;2020-02-11;12;7|13:30:00|14:15:00;Conchi|Medina|MEDI;Spanisch|SPAN;A206 SB;||;|;null;null;Klasse frei\n1;2020-02-12;13;6|12:15:00|13:00:00;Jochen|Schalon|SCAL;Geschichte|GESC;A211 SB;||;|;null;null;Klasse frei\n1;2020-02-10;13;1|08:00:00|08:45:00;Sandra|Schubert|SCHU;Geschichte|GESC;303   SB;||;|;null;null;Klasse frei\n1;2020-02-11;12;8|14:15:00|15:00:00;Conchi|Medina|MEDI;Spanisch|SPAN;A206 SB;||;|;null;null;Klasse frei\n2;2020-02-12;1051;1|08:00:00|08:45:00;Sandra|Schubert|SCHU;BBUN|BBUN;303   SB;||;|;null;null;Klasse frei";
  VDaten test1 = VDaten(
      "2",
      "2020-02-12",
      "1261",
      "2|08:45:00|09:30:00",
      "J?rgen|Morbe|MORB",
      "BBUN|BBUN",
      "A305  SB",
      "||",
      "|",
      null,
      "verschoben auf Mi, 12.02. 4. Std.",
      null
  );
  VDaten test2 = VDaten(
      "3",
      "2020-02-10",
      "1082",
      "6|12:15:00|13:00:00",
      "Brigitte|Schuchert|SCHT",
      "KLSTD|KLSTD",
      "A105 DV SB",
      "||",
      "|",
      null,
      null,
      "Klasse frei"
  );


  List<String> entrysPerHour = [
    "Klasse",
    "Stunde",
    "Lehrer",
    "Fach",
    "Raum"
    "Vertretungsfach",
    "Vertretungslehrer",
    "Vertretungsraum",
    "Merkmal",
    "Info"
  ];

  bool x = false;
  List<String> dates;
  var aktuelleDatenAll;

  /*
  Map<String, List<String>> aktuelleDatenInGrid1 = {
    "schule": [],
    "datum": [],
    "klasse": [],
    "stunde": [],
    "lehrer": [],
    "fach": [],
    "raum": [],
    "vlehrer": [],
    "vfach": [],
    "vraum": [],
    "merkmal": [],
    "info": []
  };
  Map<String, List<String>> aktuelleDatenInGrid2 = {
    "schule": [],
    "datum": [],
    "klasse": [],
    "stunde": [],
    "lehrer": [],
    "fach": [],
    "raum": [],
    "vlehrer": [],
    "vfach": [],
    "vraum": [],
    "merkmal": [],
    "info": []
  };
  Map<String, List<String>> aktuelleDatenInGrid3 = {
    "schule": [],
    "datum": [],
    "klasse": [],
    "stunde": [],
    "lehrer": [],
    "fach": [],
    "raum": [],
    "vlehrer": [],
    "vfach": [],
    "vraum": [],
    "merkmal": [],
    "info": []
  };
   */

  List<VDaten> aktuelleDaten1 = List<VDaten>();
  List<VDaten> aktuelleDaten2 = List<VDaten>();
  List<VDaten> aktuelleDaten3 = List<VDaten>();

  List<VDaten> aktuelleDaten = List<VDaten>();


  //2;2020-02-12;1261;2|08:45:00|09:30:00;J?rgen|Morbe|MORB;BBUN|BBUN;A305  SB;||;|;null;verschoben auf Mi, 12.02. 4. Std.;null
  //3;2020-02-10;1082;6|12:15:00|13:00:00;Brigitte|Schuchert|SCHT;KLSTD|KLSTD;A105 DV SB;||;|;null;null;Klasse frei
  //1;2020-02-11;12;7|13:30:00|14:15:00;Conchi|Medina|MEDI;Spanisch|SPAN;A206 SB;||;|;null;null;Klasse frei
  //1;2020-02-12;13;6|12:15:00|13:00:00;Jochen|Schalon|SCAL;Geschichte|GESC;A211 SB;||;|;null;null;Klasse frei
  //1;2020-02-10;13;1|08:00:00|08:45:00;Sandra|Schubert|SCHU;Geschichte|GESC;303   SB;||;|;null;null;Klasse frei
  //1;2020-02-11;12;8|14:15:00|15:00:00;Conchi|Medina|MEDI;Spanisch|SPAN;A206 SB;||;|;null;null;Klasse frei
  //2;2020-02-12;1051;1|08:00:00|08:45:00;Sandra|Schubert|SCHU;BBUN|BBUN;303   SB;||;|;null;null;Klasse frei

  //DataCell(Text('schule')),
  //DataCell(Text('datum')),
  //DataCell(Text('klasse')),
  //DataCell(Text('stunde')),
  //DataCell(Text('lehrer')),
  //DataCell(Text('fach')),
  //DataCell(Text('raum')),
  //DataCell(Text('vlehrer')),
  //DataCell(Text('vfach')),
  //DataCell(Text('vraum')),
  //DataCell(Text('merkmal')),
  //DataCell(Text('info')),

  MyApp myApp = new MyApp();
  var refreshKey = GlobalKey<RefreshIndicatorState>();
  Future<Null> _data;

  Future<Null> handleRefresh() async {
    refreshKey.currentState?.show(atTop: false);
    await new Future.delayed(new Duration(seconds: 1));
    connectToServer();
    return null;
  }

  Future<Null> _whileConnectToServer() async {
    while (true) {
      connectToServer();
      await Future.delayed(Duration(seconds: 5));
    }
  }

  void connectToServer() {
    Future<Socket> future = Socket.connect('45.85.217.201', 1111);
    future.then((client) {
      print('Client wurde mit dem Server verbunden!');
      client.handleError((data) {
        print(data);
      });
      client.listen((data) {
        String.fromCharCodes(data);
        if (String.fromCharCodes(data) != "\n") {
          //aktuelleDaten = String.fromCharCodes(data);
          dataVerarbeitung();
        }
        //print(String.fromCharCodes(data));
        //print(aktuelleDaten);
      },
        onDone: () {
          print("Done");
        },
        onError: (error) {
          print(error);
        },
      );
    }).catchError((Object error) {
      print('Error connecting');
    });
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: _buildListe(),
    );
  }

  void initState() {
    super.initState();
    dataVerarbeitung();
    //_data = _whileConnectToServer();
  }

  dataVerarbeitung() {
    List<int> vdates = List();

    /*
    void sort1(var x, int i) {


      x = x.split(";");

      var numbersFixed = x[1].split('');

      List<String> numbersNotFixed = [];
      numbersNotFixed.addAll(numbersFixed);

      bool isNumber(String item){
        return '0123456789'.split("").contains(item);
      }
      numbersNotFixed.removeWhere((item) => !isNumber(item));

      String numbersNotFixedAsString = "";

      for(String s in numbersNotFixed){
        numbersNotFixedAsString += s;
      }

      numbersNotFixedAsString = numbersNotFixedAsString.substring(6);
      int numbersNotFixedAsInt = int.parse(numbersNotFixedAsString);

      vdates[i] = numbersNotFixedAsInt;

    }

     */

    void sort1(VDaten x, int i) {

      var numbersFixed = x.datum.split("");

      List<String> numbersNotFixed = List();
      numbersNotFixed.addAll(numbersFixed);

      bool isNumber(String item) {
        return '0123456789'.split("").contains(item);
      }
      numbersNotFixed.removeWhere((item) => !isNumber(item));

      String numbersNotFixedAsString = "";

      for (String s in numbersNotFixed) {
        numbersNotFixedAsString += s;
      }

      numbersNotFixedAsString = numbersNotFixedAsString.substring(6);
      int numbersNotFixedAsInt = int.parse(numbersNotFixedAsString);

      vdates[i] = numbersNotFixedAsInt;
    }

    void sort2() {

      /// GETDATESNOW

      //dates = MainState().dates();
      dates = ["10", "11", "12"];


      /// COMPARE

      for (int i = 0; i < vdates.length; i++) {
        if (int.parse(dates[0]) == vdates[i]) {
          aktuelleDaten1.add(aktuelleDaten[i]);
        } else if (int.parse(dates[1]) == vdates[i]) {
          aktuelleDaten2.add(aktuelleDaten[i]);
        } else if (int.parse(dates[2]) == vdates[i]) {
          aktuelleDaten3.add(aktuelleDaten[i]);
        }
      }


    }

      /*
    void add(var x) {

      x = x.split(";");

      String schule = x[0];
      String datum = x[1];
      String klasse = x[2];
      String stunde = x[3];
      String lehrer = x[4];
      String fach = x[5];
      String raum = x[6];
      String vlehrer = x[7];
      String vfach = x[8];
      String vraum = x[9];
      String merkmal = x[10];
      String info = x[11];

      aktuelleDatenInGrid['schule'].add(schule);
      aktuelleDatenInGrid['datum'].add(datum);
      aktuelleDatenInGrid['klasse'].add(klasse);
      aktuelleDatenInGrid['stunde'].add(stunde);
      aktuelleDatenInGrid['lehrer'].add(lehrer);
      aktuelleDatenInGrid['fach'].add(fach);
      aktuelleDatenInGrid['raum'].add(raum);
      aktuelleDatenInGrid['vlehrer'].add(vlehrer);
      aktuelleDatenInGrid['vfach'].add(vfach);
      aktuelleDatenInGrid['vraum'].add(vraum);
      aktuelleDatenInGrid['merkmal'].add(merkmal);
      aktuelleDatenInGrid['info'].add(info);

    }

     */

      //aktuelleDaten = aktuelleDaten.split("\n");

      for (int i = 0; i < aktuelleDaten.length; i++) {
        sort1(aktuelleDaten[i], i);
      }

      sort2();

    aktuelleDaten.add(test1);
    aktuelleDaten.add(test2);

/*
      print(aktuelleDaten);
      print(aktuelleDaten1.toString());
      print(aktuelleDaten2.toString());
      print(aktuelleDaten3.toString());

 */

    }




    Widget _buildListe() {
      return ListView.builder(
          physics: BouncingScrollPhysics(),
          //itemCount: test?.length,
          itemCount: aktuelleDaten.length,
          padding: const EdgeInsets.all(8.0),
          itemBuilder: (context, i) {
            return _buildReihe(aktuelleDaten[i]);
          }
      );
    }

    Widget _buildReihe(VDaten tile) {
      return Card(
        color: Colors.grey,
        child: ListTile(
          title: Text(
            tile.klasse,
            style: TextStyle(fontSize: 18.0),
          ),
          leading: Icon(CupertinoIcons.news, size: 48.0, color: Colors.black,),
          subtitle: Text(tile.fach),
          trailing: Icon(Icons.chevron_right),
          onTap: () {
            Navigator.of(context).push(
              MaterialPageRoute<void>(
                builder: (BuildContext context) {
                  return Scaffold(
                    appBar: AppBar(
                      title: Text(tile.klasse),
                      actions: <Widget>[
                        IconButton(
                          icon: Icon(Icons.share),
                          tooltip: "Teile die Vertretung",
                          onPressed: () {},
                        )
                      ],
                    ),
                    body: buildListeIn(tile),
                  );
                }
              )
            );
          },
        ),
      );
    }

  Widget buildListeIn(VDaten tile) {

    List<String> dataPerHour = [
      tile.klasse,
      tile.stunde,
      tile.lehrer,
      tile.fach,
      tile.raum,
      tile.vfach,
      tile.vlehrer,
      tile.vraum,
      tile.merkmal,
      tile.info
    ];

    List<List<dynamic>> finalData = List();

    if(tile.klasse != null && tile.klasse != "" && tile.klasse is String) {
      var temp = ["Klasse", tile.klasse, Icon(Icons.class_, size: 48.0, color: Colors.black)];
      finalData.add(temp);
    }
    if(tile.stunde != null && tile.stunde != "" && tile.stunde is String) {
      var temp = ["Stunde", tile.stunde, Icon(Icons.alarm, size: 48.0, color: Colors.black)];
      finalData.add(temp);
    }
    if(tile.lehrer != null && tile.lehrer != "" && tile.lehrer is String) {
      var temp = ["Lehrer", tile.lehrer, Icon(Icons.person, size: 48.0, color: Colors.black)];
      finalData.add(temp);
    }
    if(tile.fach != null && tile.fach != "" && tile.fach is String) {
      var temp = ["Fach", tile.fach, Icon(Icons.event_busy, size: 48.0, color: Colors.black)];
      finalData.add(temp);
    }
    if(tile.raum != null && tile.raum != "" && tile.raum is String) {
      var temp = ["Raum", tile.raum, Icon(Icons.room, size: 48.0, color: Colors.black)];
      finalData.add(temp);
    }
    if(tile.vfach != null && tile.vfach != "" && tile.vfach is String) {
      var temp = ["Vertretungsfach", tile.vfach, Icon(Icons.event_available, size: 48.0, color: Colors.black)];
      finalData.add(temp);
    }
    if(tile.vlehrer != null && tile.vlehrer != "" && tile.vlehrer is String) {
      var temp = ["Vertrtungslehrer", tile.vlehrer, Icon(Icons.person_outline, size: 48.0, color: Colors.black)];
      finalData.add(temp);
    }
    if(tile.vraum != null && tile.vraum != "" && tile.vraum is String) {
      var temp = ["Vertretungsraum", tile.vraum, Icon(Icons.room, size: 48.0, color: Colors.black)];
      finalData.add(temp);
    }
    if(tile.merkmal != null && tile.merkmal != "" && tile.merkmal is String) {
      var temp = ["Merkmal", tile.merkmal, Icon(Icons.note_add, size: 48.0, color: Colors.black)];
      finalData.add(temp);
    }
    if(tile.info != null && tile.info != "" && tile.info is String) {
      var temp = ["Info", tile.info, Icon(Icons.info, size: 48.0, color: Colors.black)];
      finalData.add(temp);
    }

    return ListView.builder(
        physics: BouncingScrollPhysics(),
        //itemCount: test?.length,
        itemCount: finalData.length,
        padding: const EdgeInsets.all(8.0),
        itemBuilder: (context, i) {
          return _buildReiheIn(finalData[i]);
        }
    );
  }

  Widget _buildReiheIn(List data) {

    return Card(
      color: Colors.grey,
      child: ListTile(
        title: Text(
          data[1].toString(),
          style: TextStyle(fontSize: 18.0),
        ),
        leading: data[2],
        subtitle: Text(data[0].toString()),
        onTap: null,
      ),
    );
  }
  
}

class Test extends StatefulWidget {
  @override
  TestState createState() => TestState();
}